FROM adoptopenjdk:11-jre-openj9

MAINTAINER zhoumaoen

ENV TIME_ZONE Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TIME_ZONE /etc/localtime

RUN mkdir -p /spider-flow

WORKDIR /spider-flow

EXPOSE 8088

ADD ./spider-flow-web/target/spider-flow.jar ./

CMD sleep 30;java -Djava.security.egd=file:/dev/./urandom -jar spider-flow.jar --spring.profiles.active=pro